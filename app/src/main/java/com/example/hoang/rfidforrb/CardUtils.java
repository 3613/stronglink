package com.example.hoang.rfidforrb;

/**
 * Created by hoang on 31/01/2018.hihi
 */

public class CardUtils {
    public static String readCardFromByteArray(byte[] epc) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < epc.length; i++) {
            builder.append(String.format("%02X", epc[i]));
            if ((i + 1) % 4 == 0) builder.append(" ");
        }
        return builder.toString();
    }
}
