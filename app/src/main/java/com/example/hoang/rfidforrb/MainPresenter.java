package com.example.hoang.rfidforrb;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ivrjack.ru01.IvrJackAdapter;
import com.ivrjack.ru01.IvrJackService;
import com.ivrjack.ru01.IvrJackStatus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static com.example.hoang.rfidforrb.CameraUtils.getCameraInstance;

/**
 * Created by hoang on 31/01/2018.hihi
 */

public class MainPresenter implements IMainPresenter {
    private StorageReference mStorageRef;
    String statusText = "hu";


    Context context;

    private static final String TAG = "MainPresenter";
    private IMainView iMainView;

    public MainPresenter(Context context, IMainView iMainView) {
        this.context = context;
        this.iMainView = iMainView;

    }

    @Override
    public void capture() {


    }

    @Override
    public void showSurface(Camera mCamera, CameraPreview mPreview) {
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(context, mCamera);
        iMainView.showSurface();
    }

    @Override
    public void checkStatus(IvrJackStatus ivrJackStatus) {
        switch (ivrJackStatus) {
            case ijsDetecting:
                statusText = "ijsDetecting";
                break;
            case ijsRecognized:
                statusText = "ijsRecognized";
                break;
            case ijsUnRecognized:
                statusText = "ijsUnRecognized";

            case ijsPlugout:
                statusText = "ijsPlugout";
                break;
            default:
                statusText = "default";
                break;
        }
        iMainView.onStatusChanging(statusText);
    }

    @Override
    public void checkCameraAvailable() {
        if (CameraUtils.checkCameraHardware(context)) {
            iMainView.cameraAvailableOrNot("OK");
        } else {
            iMainView.cameraAvailableOrNot("false");

        }
    }

    @Override
    public void uploadToFirebase(byte[] bytes) {
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mStorageRef.child("mountains.jpg");
        StorageReference mountainImagesRef = mStorageRef.child("images/redbean.jpg" + System.currentTimeMillis());


        UploadTask uploadTask = mountainImagesRef.putBytes(bytes);
        Log.d("byte", CardUtils.readCardFromByteArray(bytes) + "");
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Handle unsuccessful uploads
                iMainView.onUploadFailed(e.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                iMainView.onUploadSuccess();

            }
        });

    }


    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                Log.d(TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }





}
