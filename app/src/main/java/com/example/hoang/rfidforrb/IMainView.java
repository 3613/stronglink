package com.example.hoang.rfidforrb;

import android.widget.FrameLayout;

/**
 * Created by hoang on 31/01/2018.hihi
 */

public interface IMainView {
    void onCaptured();

    void showSurface();

    void onStatusChanging(String mesages);

    void cameraAvailableOrNot(String status);

    void onUploadSuccess();

    void onUploadFailed(String e);
}
