package com.example.hoang.rfidforrb;

import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.widget.ImageView;

import com.ivrjack.ru01.IvrJackStatus;

/**
 * Created by hoang on 31/01/2018.hihi
 */

public interface IMainPresenter {
    void capture();

    void showSurface(Camera camera, CameraPreview cameraPreview);

    void checkStatus(IvrJackStatus ivrJackStatus);

    void checkCameraAvailable();


    void uploadToFirebase(byte[] bytes);
}
